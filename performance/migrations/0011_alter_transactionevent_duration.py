# Generated by Django 4.2.1 on 2023-06-04 16:42

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("performance", "0010_alter_span_id_alter_transactiongroup_id"),
    ]

    operations = [
        migrations.RemoveField("transactionevent", "duration"),
        migrations.AddField(
            model_name="transactionevent",
            name="duration",
            preserve_default=False,
            field=models.PositiveIntegerField(
                db_index=True, default=0, help_text="Milliseconds"
            ),
        ),
    ]
